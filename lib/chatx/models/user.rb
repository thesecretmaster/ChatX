require_relative 'helpers'

module ChatX
  class User
    attr_reader :owns_rooms, :in_rooms, :description, :id, :username, :server

    def initialize(server, **opts)
      if opts.values_at(:user_id).any?(&:nil?)
        raise ArgumentError, 'Got nil for an expected user property'
      end

      @server = server
      @id = opts[:user_id]

      metadata
    end

    private

    def metadata
      metadata_page = Nokogiri::HTML open("https://chat.#{@server}.com/users/#{@id}")
      @username = metadata_page.css('.usercard-xxl .user-status').first.text
      @description = metadata_page.css('.user-stats tr:nth-child(4) td:last-child').text

      in_room_cards = metadata_page.css('#user-roomcards-container').children
      @in_rooms = if !in_room_cards.nil?
                    in_room_cards.reject { |e| e.is_a? Nokogiri::XML::Text }.map do |e|
                      rid = e.attr('id').split('-').last.to_i
                      ChatX::Helpers.cached rid, :rooms do
                        Room.new @server, room_id: rid
                      end
                    end
                  else
                    []
                  end

      owns_room_cards = metadata_page.css('#user-owningcards').children[-1..1]
      @owns_rooms = if !owns_room_cards.nil?
                      owns_room_cards.reject { |e| e.is_a? Nokogiri::XML::Text }.map do |e|
                        puts e
                        rid = e.attr('id').split('-').last.to_i
                        ChatX::Helpers.cached rid, :rooms do
                          Room.new @server, room_id: rid
                        end
                      end
                    else
                      []
                    end
    end
  end
end
