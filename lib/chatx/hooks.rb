class ChatBot
  # The immediate exit point when a message is recieved from a websocket. It
  # grabs the relevant hooks, creates the event, and passes the event to the
  # hooks.
  #
  # It also spawns a new thread for every hook. This could lead to errors later,
  # but it prevents 409 errors which shut the bot up for a while.
  #
  # @note This method is strictly internal.
  # @param data [Hash] It's the JSON passed by the websocket
  def handle(data, server:)
    dmap = data.select { |k, v| k[0] == 'r' && v['e'] }.map { |k, v| v['e'] }.flatten.uniq { |e| e.tap { |e| e.delete('id') } }
    dmap.each do |e|
      event_type = e['event_type'].to_i - 1
      room_id = e['room_id']
      event = ChatX::Event.new e, server, self
      @ws_json_logger.info "#{event.type_long}: #{event.hash}"
      @ws_json_logger.info "Currently in rooms #{@rooms.keys} / #{current_rooms}"
      next if @rooms[room_id].nil?
      @rooms[room_id.to_i][:events].push(event)
      @hooks[server] ||= {}
      (Array(@hooks[server][event_type.to_i])+Array(@hooks[server]['*'])+Array(@hooks['*'][event_type.to_i])).each do |rm_id, hook|
        Thread.new do
          @hook.current_room = room_id
          hook.call(event, room_id) if rm_id == room_id || rm_id == '*'
        end
      end
    end
  end

  # A convinant way to hook into an event.
  # @param room_id [#to_i] The ID of th room to listen in.
  # @param event [String] The [EVENT_SHORTHAND] for the event ID.
  # @param action [Proc] This is a block which will run when the hook
  #   is triggered. It is passed one parameter, which is the event.
  #   It is important to note that it will NOT be passed an {Event},
  #   rather, it will be passed a sub event designated in {Event::EVENT_CLASSES}.
  def add_hook(room_id, event, server: @default_server, &action)
    @hooks[server] ||= {}
    @hook ||= Hook.new(self)
    if event == '*'
      @hooks[server]['*'] ||= []
      @hooks[server]['*'].push [room_id, action]
    else
      @hooks[server][EVENT_SHORTHAND.index(event)] ||= []
      @hooks[server][EVENT_SHORTHAND.index(event)].push [room_id, action]
    end
  end

  # A simpler syntax for creating {add_hook} to the "Message Posted" event.
  # @param room_id [#to_i] The room to listen in
  # @see #add_hook
  def on_message(room_id)
    add_hook(room_id, 'Message Posted') { |e| yield(e.hash['content']) }
  end

  # This opens up the DSL created by {Hook}.
  def gen_hooks(&block)
    @hook ||= Hook.new(self)
    @hook.instance_eval(&block)
  end
end

class Hook
  attr_reader :bot
  attr_accessor :current_room

  def initialize(bot, server: nil)
    @bot = bot
    @default_server = server || @bot.default_server
  end

  def say(message, room_id = @current_room)
    @bot.say(message, room_id)
  end

  def room(room_id, autojoin: true, server: @default_server, &block)
    room = Room.new(room_id, self, server: server)
    @bot.join_room(room_id.to_i, server: server) unless !autojoin || @bot.rooms.keys.include?(room_id)
    room.instance_eval(&block)
  end

  def on(event, server: @default_server, &block)
    @bot.hooks[server] ||= {}
    @bot.hooks[server][EVENT_SHORTHAND.index(event)] ||= []
    @bot.hooks[server][EVENT_SHORTHAND.index(event)].push ['*', block]
  end

  def command(prefix, &block)
    on "message" do |e|
      #                                        strip &zwnj;
      msg = HTMLEntities.new.decode(e.content).remove("\u200C").remove("\u200B")
      if msg.downcase == prefix || msg.downcase.start_with?("#{prefix} ")
        args = msg.scan(%r{\"(.*)\"|\'(.*)\'|([^\s]*)}).flatten.reject { |a| a.to_s.empty? }[1..-1]
        block.call(*args)
      end
    end
  end

  class Room < Hook
    def initialize(room_id, hook, server: nil)
      @hook = hook
      @bot = hook.bot
      @room_id = room_id
      @default_server = server || @bot.default_server
    end

    def say(msg)
      @bot.say(msg, @room_id, server: @default_server)
    end

    def reply_to(msg, reply)
      msg.reply(@bot, reply)
    end

    def on(event, server: @default_server, &block)
      @bot.hooks[server] ||= {}
      @bot.hooks[server][EVENT_SHORTHAND.index(event)] ||= []
      @bot.hooks[server][EVENT_SHORTHAND.index(event)].push [@room_id, block]
    end
  end
end
