class AnonClient
  attr_accessor :rooms
  attr_reader :server
  def initialize(bot, server, room_id, poll_frequency: 2)
    @poll_frequency = poll_frequency
    @bot = bot
    @dead = false
    @server = server
    join_room room_id
    start_loop
  end

  def join_room(room_id)
    @fkey = Nokogiri::HTML(open("https://chat.#{server}.com/rooms/#{room_id}").read).search("//input[@name='fkey']").attribute("value")
    events_json = Net::HTTP.post_form(URI("https://chat.#{@server}.com/chats/#{room_id}/events"),
                                      fkey: @fkey,
                                      since: 0,
                                      mode: "Messages",
                                      msgCount: 100).body

    events = JSON.parse(events_json)["events"]
    last_event_time = events.max_by { |event| event['time_stamp'] }['time_stamp']
    @rooms = {"r#{room_id}" => last_event_time}
  end

  def kill
    @dead = true
    @thread.join
  end

  private

  def start_loop
    @thread = Thread.new do
      until @dead
        response_json = Net::HTTP.post_form(URI("https://chat.#{@server}.com/events"), {fkey: @fkey}.merge(@rooms))
        response = JSON.parse(response_json.body)
        response.each do |room, data|
          @rooms[room] = data["t"] unless data["t"].nil?
        end
        @bot.handle(response)
        sleep @poll_frequency
      end
    end
  end
end
