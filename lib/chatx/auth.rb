class ChatBot
  def authenticate(sites = ["stackexchange"], login_domain: 'stackapps.com')
    login_page = @agent.get "https://#{login_domain}/users/login"
    fkey_input = login_page.search "//input[@name='fkey']"
    if fkey_input.empty?
      @logger.warn "Failed to get login fkey. Attempting to continue..."
      fkey = ''
    else
      fkey = fkey_input.attribute("value")
    end

    next_page = @agent.post("https://#{login_domain}/users/login",
                fkey: fkey,
                email: @email,
                password: @password)

    throw 'Recieved captcha. Cannot proceed' if next_page.iframes.any? { |iframe| iframe.src.include? 'captcha' }

    pp form = next_page.form(id: 'logout-user')
    if form
      @logger.info "Creating account on #{login_domain}"
      form.submit
    end

    tokens = JSON.parse(@agent.post("https://#{login_domain}/users/login/universal/request").body)

    tokens.each do |site_token|
      @agent.get("https://#{site_token['Host']}/users/login/universal.gif", {authToken: site_token['Token'], nonce: site_token['Nonce']}, "https://#{login_domain}", {'Referer' => "https://#{login_domain}/"})
    end

    sites.all? do |site|
      home = @agent.get "https://chat.#{site}.com"
      if home.search(".topbar-links span.topbar-menu-links a").first.text.casecmp('log in').zero?
        @logger.warn "Login to #{site} failed :("
        false
      else
        @logger.info "Login to #{site} successful!"
        true
      end
    end
  end

  def openid_authenticate(sites = ["stackexchange"])
    if sites.is_a? Hash
      sites.each do |site, cookie_str|
        cookie = Mechanize::Cookie.new("acct", cookie_str)
        cookie.domain = ".#{site}.com"
        cookie.path = "/"
        @agent.cookie_jar.add!(cookie)
      end
      true
    else
      @logger.warn "Using openid authentication is deprecated! It will cease to work after July 25th, 2018."
      sites = [sites] unless sites.is_a?(Array)

      openid = @agent.get "https://openid.stackexchange.com/account/login"
      fkey_input = openid.search "//input[@name='fkey']"
      fkey = fkey_input.empty? ? "" : fkey_input.attribute("value")

      @agent.post("https://openid.stackexchange.com/account/login/submit",
                  fkey:     fkey,
                  email:    @email,
                  password: @password)

      auth_results = sites.map { |s| openid_site_auth(s) }
      failed = auth_results.any?(&:!)
      !failed
    end
  end

  def openid_site_auth(site)
    @logger.warn "Using openid authentication is deprecated! It will cease to work after July 25th, 2018."
    # Get fkey
    login_page = @agent.get "https://#{site}.com/users/login"
    fkey_input = login_page.search "//input[@name='fkey']"
    fkey = fkey_input.attribute('value')

    @agent.post("https://#{site}.com/users/authenticate",
                fkey: fkey,
                openid_identifier: 'https://openid.stackexchange.com')

    home = @agent.get "https://chat.#{site}.com"
    if home.search(".topbar-links span.topbar-menu-links a").first.text.casecmp('log in').zero?
      @logger.warn "Login to #{site} failed :("
      false
    else
      @logger.info "Login to #{site} successful!"
      true
    end
  end
end
