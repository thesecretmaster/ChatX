require 'date'
require_relative 'helpers'
require_relative 'room'
require_relative 'user'

module ChatX
  class Message
    attr_reader :server, :timestamp, :content, :room, :user, :id, :parent_id

    def initialize(server, **opts)
      if opts.values_at(:time_stamp, :content, :room_id, :user_id, :message_id).any?(&:nil?)
        raise ChatX::InitializationDataException, 'Got nil for an expected message property'
      end

      @server = server

      @id = opts[:message_id]
      @timestamp = Time.at(opts[:time_stamp]).utc.to_datetime
      @content = opts[:content]
      @room = ChatX::Helpers.cached opts[:room_id].to_i, :rooms do
        ChatX::Room.new server, room_id: opts[:room_id].to_i
      end
      @user = ChatX::Helpers.cached opts[:user_id].to_i, :users do
        ChatX::User.new server, user_id: opts[:user_id].to_i
      end

      @parent_id = opts[:parent_id]
    end

    alias_method :body, :content

    def reply(bot, content)
      bot.say ":#{id} #{content}", @room.id, server: @server
    end

    def reply?
      @content =~ /^:\d+\s/
    end

    def pings
      @pings = @content.scan(/@(\w+)/).flatten if @pings.nil?
      @pings
    end

    def pinged?(username)
      @pings.map(&:downcase).map { |x| username.downcase.start_with? x }.any?
    end

    %i[
      toggle_star
      star_count
      star
      unstar
      starred?
      cancel_stars
      delete
      edit
      toggle_pin
      pin
      unpin
      pinned?
    ].each do |name|
      define_method(name) { |bot| bot.send(name, @id, @server) }
    end

    def self.from_hash(server, hash)
      new server, **ChatX::Helpers.symbolize_hash_keys(hash)
    end
  end
end
