require_relative 'helpers'

module ChatX
  class Room
    attr_reader :users, :name, :id, :server, :stars, :owners, :description

    def initialize(server, **opts)
      if opts.values_at(:room_id).any?(&:nil?)
        raise ArgumentError, 'Got nil for an expected room property'
      end

      @server = server

      @id = opts[:room_id]
      track_users            if opts[:track_users]
      track_starred_messages if opts[:track_starred_messages]
      metadata               if opts[:metadata]
    end

    private

    def track_users
      room_page = Nokogiri::HTML open("https://chat.#{@server}.com/rooms/#{@id}")
      @users = room_page.css('#room-usercards-container').children.map do |e|
        e = e.css('.user-header > a').attr('href').split('/')
        User.new(server,
                 user_name: e[-1],
                 user_id:   e[-2])
      end
    end

    def track_starred_messages
      star_page = Nokogiri::HTML open("https://chat.#{@server}.com/rooms/starred/#{@id}")
      @stars = star_page.css('entry').map do |e|
        Message.new @server,
                    time_stamp:  Time.parse(e.css('published').first.text).utc.to_i,
                    content:    e.css('summary').first.text,
                    user_id:    e.css('author uri').first.text.split('/').last,
                    message_id: e.css('id').first.text.split('-').last,
                    room_id:    @id
      end
    end

    def metadata
      metadata_card = Nokogiri::HTML(open("https://chat.#{@server}.com/rooms/info/#{@id}")).css('.roomcard-xxl')
      @name = metadata_card.css('h1').first.text
      @description = metadata_card.css('p').first.text

      owner_cards = Nokogiri::HTML(open("https://chat.#{@server}.com/rooms/info/#{@id}")).css('.room-ownercards')
      @owners = owner_cards.each do |e|
        id, name = e.css('a:first-child').attr('href').split('/')
        User.new(@server, user_id: id, user_name: name)
      end
    end
  end
end
