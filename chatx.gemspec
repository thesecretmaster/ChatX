# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "chatx/version"

Gem::Specification.new do |spec|
  spec.name          = "chatx"
  spec.version       = ChatBot::VERSION
  spec.authors       = ["izwick-schachter"]
  spec.email         = ["isaiah@developingtechnician.com"]

  spec.summary       = %q{A bot. For the chat. This bot is a ruby way to chat with other
    people on chat.stackexchange.com, chat.meta.stackexchange.com, or chat.stackoverflow.com}
  spec.description   = %q{A chatbot! For SE! In ruby!}
  spec.homepage      = "https://gitlab.com/izwick-schachter/chatx"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "rubocop", "~> 0.49"
  spec.add_development_dependency "yard", "~> 0.9"
  spec.add_development_dependency 'byebug', '~> 9.0'
  spec.add_development_dependency 'guard', '~> 2.14'
  spec.add_development_dependency 'guard-minitest', '~> 2.4'

  [
    ["mechanize",          "~> 2.7"],
    ["nokogiri",           "~> 1.8"],
    ["permessage_deflate", "~> 0.1"],
    ["websocket-driver",   "~> 0.6"],
    ["htmlentities",       "~> 4.3"]
  ].each do |g, v|
    spec.add_runtime_dependency g, v
  end
end
