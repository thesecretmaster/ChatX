require "test_helper"

describe ChatBot do
  before do
    # Set CXUsername and CXPassword in GitLab Pipelines settings (Repo > Settings > Pipelines).
    @bot = ChatBot.new ENV['CXUsername'], ENV['CXPassword'], log_location: 'test.log'
    assert @bot.login
    refute ChatBot.new("not-a-real-account@example.com", "correct horse battery staple", log_location: 'test.log').login
  end

  after do
    @bot.kill
  end

  def alternate_bot
    # Set CXUsername and CXPassword in GitLab Pipelines settings (Repo > Settings > Pipelines).
    bot = ChatBot.new ENV['CXUsername'], ENV['CXPassword'], log_location: 'test.log'
    assert bot.login
    bot
  end

  it 'should open a new socket for each new room' do
    assert @bot.websockets.is_a? Hash
    assert_nil @bot.websockets["stackexchange"]
    @bot.join_room(63296)
    refute_nil @bot.websockets["stackexchange"]
    prev_ws = @bot.websockets["stackexchange"]
    @bot.join_room(63561)
    refute_equal prev_ws, @bot.websockets["stackexchange"]
    refute prev_ws.thread.status
    assert 101, prev_ws.driver.status
    @bot.leave_room(63561)
    @bot.leave_room(63296)
  end

  it 'should join rooms' do
    @bot.join_room 63296
    last_updated  = @bot.websockets["stackexchange"].in_rooms[:last_updated]
    sleep 0.1 until last_updated !=  @bot.websockets["stackexchange"].in_rooms[:last_updated]
    assert_equal @bot.websockets["stackexchange"].in_rooms[:rooms], [63296]

    @bot.join_room 63561
    last_updated  = @bot.websockets["stackexchange"].in_rooms[:last_updated]
    sleep 0.1 until last_updated !=  @bot.websockets["stackexchange"].in_rooms[:last_updated]
    assert_includes @bot.websockets["stackexchange"].in_rooms[:rooms], 63561
    assert_includes @bot.websockets["stackexchange"].in_rooms[:rooms], 63296
    assert @bot.websockets["stackexchange"].in_rooms[:rooms].length == 2

    @bot.leave_room 63296
    last_updated  = @bot.websockets["stackexchange"].in_rooms[:last_updated]
    sleep 0.1 until last_updated !=  @bot.websockets["stackexchange"].in_rooms[:last_updated]
    assert_equal @bot.websockets["stackexchange"].in_rooms[:rooms], [63561]

    @bot.leave_room 63561
  end

  it 'should post messages' do
    alt_bot = alternate_bot
    alt_bot.gen_hooks do
      room 63561 do
        on "message" do
          $working = true
        end
      end
    end
    anon = AnonClient.new(alt_bot,"stackexchange", 63561)
    @bot.join_room 63561
    @bot.say("I'm running the test suite!", 63561)
    sleep 0.1 until $working
    assert $working
    @bot.leave_room 63561
    anon.kill
  end
end
