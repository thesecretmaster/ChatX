require 'test_helper'

describe ChatX::Helpers do
  it 'should symbolize hash keys' do
    pre_hash = { 'a' => 'b', 'c' => 'd' }
    post_hash = ChatX::Helpers.symbolize_hash_keys pre_hash
    assert_equal({ a: 'b', c: 'd' }, post_hash)
  end

  it 'should only symbolize top-level keys' do
    pre_hash = { 'a' => { 'b' => 'c' }, 'd' => 'e' }
    post_hash = ChatX::Helpers.symbolize_hash_keys pre_hash
    assert_equal({ a: { 'b' => 'c' }, d: 'e' }, post_hash)
  end
end
