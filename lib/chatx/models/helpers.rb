module ChatX
  class Helpers
    @cache = {}

    def self.symbolize_hash_keys(hash)
      hash.each_key do |key|
        # rubocop:disable Lint/RescueWithoutErrorClass
        hash[(begin
                key.to_sym
              rescue
                key
              end) || key] = hash.delete(key)
        # rubocop:enable: Lint/RescueWithoutErrorClass
      end
      hash
    end

    def self.cached(key, scope = nil)
      @cache ||= {}
      if !scope.nil?
        @cache[scope] ||= {}
        @cache[scope][key] = yield if @cache[scope][key].nil?
        @cache[scope][key]
      else
        @cache[key] = yield if @cache[key].nil?
        @cache[key]
      end
    end
  end

  class InitializationDataException < RuntimeError
    def initialize(msg)
      super(msg)
    end
  end
end
