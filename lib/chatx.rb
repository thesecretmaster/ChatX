require "mechanize"
require "nokogiri"
require "websocket/driver"
require "json"
require "permessage_deflate"
require "open-uri"
require "htmlentities"
require_relative "chatx/events"
require_relative "chatx/auth"
require_relative "chatx/websocket"
require_relative "chatx/hooks"
require_relative "chatx/polling"

require "thwait"

Thread.abort_on_exception = true

class ChatBot
  # @!attribute [r] rooms
  #   @return [Hash<Hash<Array>>] a hash of the rooms. Each key is a room ID, and
  #     each value is futher hash with an :events key which contains an array
  #     of {Event}s from that room.
  # @!attribute [r] websocket
  #   @return [Hash<room_id, Thread>] a hash of websockets. Each key is a room ID and
  #     each value is a websocket thread. Each websocket gets it's own thead because
  #     EventMachine blocks the main thread when I run it there.
  # @!attribute [rw] default_server
  #   @return [String] The default server to connect to. It's good to
  #     pass this to the default_server keyword argument of the {initialize} method
  #     if you're only sticking to one server. Otherwise, with every {#say}
  #     you'll have to specify the server. This defaults to stackexchange. The options for
  #     this are "meta.stackexchange", "stackexchange", and "stackoverflow". The rest of
  #     the URL is managed internally.
  # @!attribute [rw] hooks
  #   @return [Array] An array of the {Hook}s for the bot.
  attr_reader :rooms, :websockets, :logger, :agent
  attr_accessor :default_server, :hooks, :logger_opts

  # Creates a bot.
  #
  # These must be stack exchange openid credentials, and the user
  # must have spoken in a room on that server first.
  #
  # For further details on authentication, see #authenticate
  #
  # @param [String] email The Stack Exchange OpenID email
  # @param [String] password The Stack Exchange OpenID password
  # @return [ChatBot] A new bot instance. Not sure what happens if
  #   you try and run more than one at a time...
  def initialize(email, password, **opts)
    opts[:default_server] ||= 'stackexchange'
    @logger_opts = {
      log_location: opts[:log_location] || STDOUT,
      log_level: opts[:log_level] || Logger::DEBUG,
      log_formatter: opts[:log_formatter] || Logger.new('/dev/null').formatter
    }

    @logger = Logger.new logger_opts[:log_location]
    @logger.level = logger_opts[:log_level]
    @logger.formatter = logger_opts[:log_formatter]

    @ws_json_logger = Logger.new 'websockets_json.log'

    # Both of these can be overriden in #login with cookie:
    @email = email
    @password = password
    @agent = Mechanize.new
    @rooms = {} # room_id => {events}
    @default_server = opts[:default_server]
    @hooks = {'*' => []}
    @websockets = {}
    at_exit { rejoin }
  end

  # Logs the bot into the three SE chat servers.
  #
  # @return [Boolean] A bool indicating the result of authentication: true if all three
  # servers were authenticated successfully, false otherwise.
  def login(servers = @default_server, cookie_file: nil)
    if cookie_file && File.exists?(cookie_file)
      @logger.info "Loading cookies from #{cookie_file}"
      @agent.cookie_jar.load(cookie_file)
    else
      servers = [servers] unless servers.is_a?(Array) || servers.is_a?(Hash)
      throw "Login failed; Exiting." unless authenticate(servers)
      if cookie_file
        @agent.cookie_jar.save(cookie_file, session: true)
      end
    end
  end

  # Attempts to join a room, and for every room joined opens a websocket.
  # Websockets seem to be the way to show your presence in a room. It's weird
  # that way.
  #
  # Each websocket is added to the @websockets instance variable which can be
  # read but not written to.
  #
  # @param room_id [#to_i] A valid room ID on the server designated by the
  #   server param.
  # @keyword server [String] A string referring to the relevant server. The
  #   default value is set by the @default_server instance variable.
  # @return [Hash] The hash of currently active websockets.
  def join_room(room_id, server: @default_server)
    @logger.info "Joining #{room_id} on server #{server}"

    fkey = get_fkey(server, "rooms/#{room_id}")

    @agent.get("https://chat.#{server}.com/rooms/#{room_id}", fkey: fkey)

    events_json = @agent.post("https://chat.#{server}.com/chats/#{room_id}/events",
                              fkey: fkey,
                              since: 0,
                              mode: "Messages",
                              msgCount: 100).body

    events = JSON.parse(events_json)["events"]

    @logger.info "Retrieved events (length #{events.length})"

    ws_auth_data = @agent.post("https://chat.#{server}.com/ws-auth",
                               roomid: room_id,
                               fkey: fkey)

    @logger.info "Began room auth for room id: #{room_id}"

    @rooms[room_id.to_i] = {}
    @rooms[room_id.to_i][:events] = events.map do |e|
      begin
        ChatX::Event.new e, server, self
      rescue ChatX::InitializationDataException => e
        @logger.warn "Caught InitializationDataException during events population (#{e}); skipping event"
        nil
      end
    end.compact

    @logger.info "Rooms: #{@rooms.keys}"

    unless @websockets[server].nil? || @websockets[server].dead
      @logger.info "Websocket #{@websockets[server]} already open; clearing."
      @websockets[server].close
      @websockets[server] = nil
    end
    @logger.info "SOOOOOOOOOOOOOOOOOOOOOOOOOOOO"
    ws_uri = JSON.parse(ws_auth_data.body)["url"]
    last_event_time = events.max_by { |event| event['time_stamp'] }['time_stamp']
    cookies = (@agent.cookies.map { |cookie| "#{cookie.name}=#{cookie.value}" if cookie.domain == "chat.#{server}.com" || cookie.domain == "#{server}.com" } - [nil]).join("; ")
    @logger.info "Launching new WSCLient"
    @websockets[server] = WSClient.new("#{ws_uri}?l=#{last_event_time}", cookies, self, server)
    @logger.info "New websocket open (#{@websockets[server]}"
  end

  def join_rooms(*rooms, server: @default_server)
    rooms.flatten.each { |rid| join_room(rid) }
  end

  # Leaves the room. Not much else to say...
  # @param room_id [#to_i] The ID of the room to leave
  # @keyword server [String] The chat server that room is on.
  # @return A meaningless value
  def leave_room(room_id, server: @default_server)
    fkey = get_fkey("stackexchange", "/rooms/#{room_id}")
    @rooms.delete(room_id) unless @rooms[room_id].nil?
    @agent.post("https://chat.#{server}.com/chats/leave/#{room_id}", fkey: fkey, quiet: "true")
  end

  # Speaks in a room! Not much to say here, but much to say in the room
  # that is passed!
  #
  # If you're trying to reply to a message, please use the {Message#reply}
  # method.
  # @param room_id [#to_i] The ID of the room to be spoken in
  # @param content [String] The text of message to send
  # @keyword server [String]  The server to send the messon on.
  # @return A meaningless value
  def say(content, room_id, server: @default_server)
    fkey = get_fkey(server, "/rooms/#{room_id}")
    if content.to_s.empty?
      @logger.warn "Message is empty, not posting: '#{content}'"
      return
    end
    if content.count("\n").zero? && content.length > 500
      @logger.warn "Message too long, truncating to 500 chars: #{content}"
      content = content[0...500]
    end
    resp ||= nil
    loop do
      begin
        resp = @agent.post("https://chat.#{server}.com/chats/#{room_id}/messages/new", fkey: fkey, text: content)
      rescue Mechanize::ResponseCodeError
        @logger.error "Posting message to room #{room_id} failed. Retrying... #{content}"
        sleep 1 # A magic number I just chose for no reason
        retry
      end
      break unless JSON.parse(resp.content)["id"].nil?
      content = " #{content}"
    end
    return JSON.parse(resp.content)["id"].to_i
  end

  def toggle_star(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/star", fkey: fkey)
  end

  def star_count(message_id, server: @default_server)
    page = @agent.get("https://chat.#{server}.com/transcript/message/#{message_id}")
    page.css("#message-#{message_id} .flash .star .times")[0].content.to_i
  end

  def star(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/star", fkey: fkey) unless starred?(message_id)
  end

  def unstar(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/star", fkey: fkey) if starred?(message_id)
  end

  def starred?(message_id, server: @default_server)
    page = @agent.get("https://chat.#{server}.com/transcript/message/#{message_id}")
    return false if page.css("#message-#{message_id} .flash .stars")[0].nil?
    page.css("#message-#{message_id} .flash .stars")[0].attr("class").include?("user-star")
  end

  def cancel_stars(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/unstar", fkey: fkey)
  end

  def delete(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    attempts = 0
    begin
      attempts += 1
      resp = @agent.post("https://chat.#{server}.com/messages/#{message_id}/delete", fkey: fkey)
    rescue Mechanize::ResponseCodeError
      @logger.error "Deleting message with ID #{message_id} failed. Retrying..."
      sleep 1 # A magic number I just chose for no reason
      if attempts >= 120
        @logger.error "Gave up trying to delete message with ID #{message_id}"
      else
        retry
      end
    end
  end

  def delete_messages(*message_ids, room_id:, server: @default_server)
    fkey = get_fkey("stackexchange")
    attempts = 0
    begin
      attempts += 1
      resp = @agent.post("https://chat.#{server}.com/admin/deletePosts/#{room_id}", fkey: fkey, id: message_ids.join(','))
    rescue Mechanize::ResponseCodeError
      @logger.error "Deleting messages with IDs #{message_ids.join(',')} failed. Retrying..."
      sleep 1 # A magic number I just chose for no reason
      if attempts >= 120
        @logger.error "Gave up trying to delete messages with IDs #{message_ids.join(',')}"
      else
        retry
      end
    end
  end

  def edit(message_id, new_message, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}", fkey: fkey, text: new_message)
  end

  def toggle_pin(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/owner-star", fkey: fkey)
  end

  def pin(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/owner-star", fkey: fkey)
  end

  def unpin(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/owner-star", fkey: fkey)
  end

  def pinned?(message_id, server: @default_server)
    page = @agent.get("https://chat.#{server}.com/transcript/message/#{message_id}")
    return false if page.css("#message-#{message_id} .flash .stars")[0].nil?
    page.css("#message-#{message_id} .flash .stars")[0].attr("class").include?("owner-star")
  end

  # Kills all active websockets for the bot.
  def kill
    @websockets.values.each(&:close)
  end

  def rejoin
    ThreadsWait.all_waits @websockets.values.map(&:thread)
    leave_all_rooms
  end

  def stop
    leave_all_rooms
    @websockets.values.map(&:thread).each(&:kill)
  end

  def leave_all_rooms
    @rooms.each_key do |room_id|
      leave_room(room_id)
    end
  end

  def current_rooms
    @websockets.map do |server, ws|
      [server, ws.in_rooms[:rooms]]
    end.to_h
  end

  private

  def get_fkey(server, uri = "")
    @agent.get("https://chat.#{server}.com/#{uri}").search("//input[@name='fkey']").attribute("value")
  rescue Mechanize::ResponseCodeError
    @logger.error "Getting fkey failed for uri https://chat.#{server}.com/#{uri}. Retrying..."
    sleep 1 # A magic number I just chose for no reason
    retry
  end
end
