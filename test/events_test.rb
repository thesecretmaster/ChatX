require 'test_helper'

describe ChatX::Event do
  before do
    @bot = ChatBot.new ENV['CXUsername'], ENV['CXPassword'], log_location: 'events_test.log'
  end

  it 'should initialize message event correctly' do
    evt = ChatX::Event.new({'time_stamp' => 1502947152, 'content' => 'test message', 'room_id' => 63296,
                            'user_id' => 300151, 'message_id' => 39355782, 'event_type' => 1}, 'stackexchange', @bot)
    refute_nil evt.message
    assert_equal 39355782, evt.message.id
    assert_equal 'test message', evt.message.content
  end

  it 'should initialize entrance event correctly' do
    evt = ChatX::Event.new({'target_user_id' => 300151, 'room_id' => 63296, 'event_type' => 3}, 'stackexchange', @bot)
    refute_nil evt.user
    assert_equal 300151, evt.user.id
    refute_nil evt.room
    assert_equal 63296, evt.room.id
  end

  it 'should initialize rename event correctly' do
    evt = ChatX::Event.new({'room_id' => 63296, 'event_type' => 5}, 'stackexchange', @bot)
    refute_nil evt.room
    assert_equal 63296, evt.room.id
  end

  it 'should initialize star event correctly' do
    evt = ChatX::Event.new({'time_stamp' => 1502947152, 'content' => 'test message', 'room_id' => 63296,
                            'user_id' => 300151, 'message_id' => 39355782, 'event_type' => 6}, 'stackexchange', @bot)
    refute_nil evt.message
    assert_equal 39355782, evt.message.id
    assert_equal 'test message', evt.message.content
  end

  it 'should initialize mention event correctly' do
    evt = ChatX::Event.new({'time_stamp' => 1502947152, 'content' => 'test message', 'room_id' => 63296,
                            'user_id' => 300151, 'message_id' => 39355782, 'target_user_id' => 121520,
                            'event_type' => 8}, 'stackexchange', @bot)
    refute_nil evt.message
    assert_equal 39355782, evt.message.id
    assert_equal 300151, evt.message.user.id
    refute_nil evt.source_user
    assert_equal 300151, evt.source_user.id
    assert_equal evt.message.user.id, evt.source_user.id
    refute_nil evt.target_user
    assert_equal 121520, evt.target_user.id
  end

  it 'should initialize reply event correctly' do
    evt = ChatX::Event.new({'time_stamp' => 1502947152, 'content' => 'test message', 'room_id' => 63296,
                            'user_id' => 300151, 'message_id' => 39355782, 'event_type' => 18}, 'stackexchange', @bot)
    refute_nil evt.message
    assert_equal 39355782, evt.message.id
    assert_equal 'test message', evt.message.content
  end
end
