# ChatX

ChatX is a more ruby version of the ChatExchange python library for interacting with
Stack Exchange Chat, Stack Overflow Chat, and Meta Stack Exchange Chat.

The first thing to do is to go and make a chat account on the servers you want to use. Then, you can start with this gem.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'chatx', git: 'https://gitlab.com/izwick-schachter/ChatX.git', branch: 'master'
```

And then execute:

    $ bundle

Or install it yourself from rubygems (NOT RECOMMENDED) as:

    $ gem install chatx
## Usage

The first thing you need is to create a new `ChatBot` and tell it what username you used, the password you used. If you
want it to operate on a site besides chat.stackexchange.com, you'll have to specify it:

```ruby
my_chatbot = ChatBot.new("smartbot@example.com", "correct horse battery staple", default_server: 'stackexchange')
```

If you want to improve your security a bit further, you could also get the username and password from environment
variables:

```bash
$ export CXUsername=smartbot@example.com
$ export CXPassword="correct horse battery staple"
```
```ruby
my_chatbot = ChatBot.new(ENV["CXUsername"], ENV["CXPassword"])
```
If you wanted to run it on a different server, you'd pass that server as the third argument to `ChatBot.new`. The
servers are given as either "stackexchange", "meta.stackexchange" or "stackoverflow".

To speak, the bot has a `say` method. For example, to have your chatbot say "Hello World" in the room with ID 1, you
would run `my_chatbot.say 1, "Hello World"`.

The recommended way to interact with chat is through "hooks". Hooks are a way of responding to actions in chat. For
example, if you wanted to have it reply to the message "!!/alive" in room 1 with "Yup, I'm here" you could create a
hook for the "message" event:

```ruby
my_chatbot.add_hook 1, "message" do
  say "Yup, I'm here"
end
```

Or, if you wanted to make that a reply to the "!!/alive" message, you could do:

```ruby
my_chatbot.add_hook 1, "message" do |msg|
  reply_to msg, "Yup, I'm here"
end
```

Here's a complete list of events that you can add a hook to:

```
"message"
"edit"
"entrance"
"exit"
"rename"
"star"
"debug"
"mention"
"flag"
"delete"
"file"
"mod-flag"
"settings"
"gnotif"
"level"
"lnotif"
"invite"
"reply"
"move-out"
"move-in"
"time"
"feed"
"suspended"
"merged"
```

The recommended way to add hooks is though the `gen_hooks` method, together with the room method:

```ruby
my_chatbot.gen_hooks do
  room 63296 do
    on "mention" do |e|
      e.reply "Right!" if e.body.downcase.end_with?("right?")
    end

    on "message" do |e|
      if e.body.downcase.start_with?("@smelly good")
        tod = e.body.split(" ")[2].gsub(/[^a-zA-Z]/, "")
        e.reply "Good #{tod} to you as well!"
      end
    end
    
    command '!!/add' do |*args|
      say args.map(&:to_i).sum
  	end
  end
end
```

Also, your hooks won't be active until you manually join the relevant rooms with `my_chatbot.join_room(1)`, and they'll
stop being active when you leave the room with `my_chatbot.leave_room(1)`.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can
also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the
version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version,
push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/izwick-schachter/ChatX. This project is
intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the
[Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the ChatX project’s codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://github.com/[USERNAME]/chatx/blob/master/CODE_OF_CONDUCT.md).
