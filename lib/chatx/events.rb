require_relative 'models/message'
require_relative 'models/room'
require_relative 'models/user'

EVENT = %w[
  Message Posted
  Message Edited
  User Entered
  User Left
  Room Name Changed
  Message Starred
  Debug Message
  User Mentioned
  Message Flagged
  Message Deleted
  File Added
  Moderator Flag
  User Settings Changed
  Global Notification
  Access Level Changed
  User Notification
  Invitation
  Message Reply
  Message Moved Out
  Message Moved In
  Time Break
  Feed Ticker
  User Suspended
  User Merged
].freeze

EVENT_SHORTHAND = %w[
  message
  edit
  entrance
  exit
  rename
  star
  debug
  mention
  flag
  delete
  file
  mod-flag
  settings
  gnotif
  level
  lnotif
  invite
  reply
  move-out
  move-in
  time
  feed
  suspended
  merge
].freeze

module ChatX
  class Event
    EVENT_CLASSES = {
      1 => {
        ChatX::Message => {
          time_stamp: 'time_stamp',
          content: 'content',
          room_id: 'room_id',
          user_id: 'user_id',
          message_id: 'message_id',
          parent_id: 'parent_id'
        }
      },
      2 => {
        ChatX::Message => {
          time_stamp: 'time_stamp',
          content: 'content',
          room_id: 'room_id',
          user_id: 'user_id',
          message_id: 'message_id'
        }
      },
      3 => {
        ChatX::User => {
          user_id: 'target_user_id'
        },
        ChatX::Room => {
          room_id: 'room_id'
        }
      },
      4 => {
        ChatX::User => {
          user_id: 'target_user_id'
        },
        ChatX::Room => {
          room_id: 'room_id'
        }
      },
      5 => {
        ChatX::Room => {
          room_id: 'room_id'
        }
      },
      # Note: A star/unstar/pin/unpin event DOES NOT have a user_id, which throws an error in ChatX::Message intialization.
      # 6 => {
      #   ChatX::Message => {
      #     time_stamp: 'time_stamp',
      #     content: 'content',
      #     room_id: 'room_id',
      #     user_id: 'user_id',
      #     message_id: 'message_id'
      #   }
      # },
      8 => {
        ChatX::Message => {
          time_stamp: 'time_stamp',
          content: 'content',
          room_id: 'room_id',
          user_id: 'user_id',
          message_id: 'message_id'
        },
        [ChatX::User, 'source_user'] => {
          user_id: 'user_id'
        },
        [ChatX::User, 'target_user'] => {
          user_id: 'target_user_id'
        }
      },
      18 => {
        ChatX::Message => {
          time_stamp: 'time_stamp',
          content: 'content',
          room_id: 'room_id',
          user_id: 'user_id',
          message_id: 'message_id'
        }
      }
    }.freeze

    attr_reader :type, :type_long, :type_short, :hash

    def initialize(event_hash, server, bot)
      @type = event_hash['event_type'].to_i
      @type_short = EVENT_SHORTHAND[@type - 1]
      @type_long = EVENT[@type - 1]
      @bot = bot

      @etypes = []

      @hash = event_hash # .delete("event_type")

      # rubocop:disable Style/GuardClause
      if EVENT_CLASSES.include? @type
        classes = EVENT_CLASSES[@type]
        classes.each do |c, i|
          if c.class == Array
            clazz = c[0]
            method_name = c[1]
          else
            clazz = c
            method_name = clazz.to_s.split('::').last.downcase
          end
          init_properties = i.map { |k, v| [k, event_hash[v]] }.to_h
          # This bit is fun. It creates a ChatX::<event type> and uses the method_missing
          # to make its methods all aliased to this ChatX::Event, and also defines an
          # <event type> method on this ChatX::Event which returns the ChatX::<event type>
          # object.
          event_type_obj = clazz.new(server, **init_properties)
          @etypes.push event_type_obj
          instance_variable_set "@#{method_name}", event_type_obj
          self.class.send(:define_method, method_name) do
            instance_variable_get "@#{method_name}"
          end
        end
      end
      # rubocop:enable Style/GuardClause

      def method_missing(m, *args, **opts, &block)
        etypes = @etypes.select { |e| e.respond_to? m }
        super if etypes.empty?
        etype = etypes.first
        if etype.respond_to? m
          meth = etype.method(m)
          # Because it treats **opts as 1 argument
          if opts.empty?
            meth.call(*args, &block)
          else
            meth.call(*args, **opts, &block)
          end
        end
      end

      def respond_to_missing?(m, *)
        !@etypes.select { |e| e.respond_to? m }.empty? || super
      end
    end
  end
end
